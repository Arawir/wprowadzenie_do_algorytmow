#include "main.h"

int main()
{
    Matrix6d Pg;
    Matrix6d Mg;
    std::list<Vector6d> Distributions;
    GnuplotPipe Gp;

    Pg << 0.0, 0.0, 0.0, 0.5, 0.5, 0.0,
          0.0, 0.0, 0.5, 0.0, 0.0, 0.5,
          1.0/6.0, 1.0/6.0, 1.0/6.0, 1.0/6.0, 1.0/6.0, 1.0/6.0,
          1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
          0.0, 0.5, 0.0, 0.5, 0.0, 0.0,
          0.0, 0.0 ,0.0, 0.0, 0.0, 1.0;

    Mg = prepareGoogleMatrix(Pg, 0.0);
    Distributions.push_back( calculateStationaryDistribution(Mg, 1e-6) );
    Mg = prepareGoogleMatrix(Pg, 0.15);
    Distributions.push_back( calculateStationaryDistribution(Mg, 1e-6) );
    Mg = prepareGoogleMatrix(Pg, 0.5);
    Distributions.push_back( calculateStationaryDistribution(Mg, 1e-6) );
    Mg = prepareGoogleMatrix(Pg, 1.0);
    Distributions.push_back( calculateStationaryDistribution(Mg, 1e-6) );

    saveDataToFile(Distributions);
    plotData(Gp);

    getchar();

    return 0;
}

