#ifndef MAIN_H
#define MAIN_H

#include <iostream>
#include <Eigen/Dense>
#include <iomanip>
#include <fstream>
#include <list>
#include "gnuplot.h"


#define output std::cout << std::fixed << std::setprecision(4)
#define Matrix6d Eigen::Matrix<double, 6, 6>
#define Vector6d Eigen::Matrix<double, 6, 1>


Matrix6d prepareGoogleMatrix(Matrix6d pg, double alpha)
{
    Matrix6d GoogleMatrix = (1.0-alpha)*pg + alpha/GoogleMatrix.rows()*Matrix6d::Ones();
    return GoogleMatrix;
}

Vector6d calculateStationaryDistribution(Matrix6d Mg, double treshold)
{
    Vector6d Distribution = Vector6d::Ones();
    Vector6d NewVector;

    for(int i=0; i<1000; i++){
        NewVector = Distribution.transpose()*Mg;
        if( (Distribution/Distribution.sum()-(NewVector/NewVector.sum())).sum() < treshold){
            return NewVector/NewVector.sum();
        }
        Distribution = NewVector;
    }

    output << "WARNING: convergence can not be achieved" << std::endl;
    return Distribution/Distribution.sum();
}


void saveDataToFile(std::list<Vector6d> Distributions)
{
    std::fstream file;
    file.open("../data/data", std::ios::out);

    for(int i=0; i<6; i++){
        for(auto &distribution : Distributions){
            file << distribution[i] << "    ";
        }
        file << std::endl;
    }

    file.close();
}


void plotData(GnuplotPipe& gp)
{
    gp.sendLine("set title 'Stationary distribution'");
    gp.sendLine("set xlabel 'Node'");
    gp.sendLine("set ylabel 'P");

    gp.sendLine("set title font 'helvetica,20'");
    gp.sendLine("set xlabel font 'helvetica,20'");
    gp.sendLine("set ylabel font 'helvetica,20'");
    gp.sendLine("set key font 'helvetica,20'");

    gp.sendLine("plot '../data/data' u 1 w l title '{/Symbol a}=0',"
                    " '../data/data' u 2 w l title '{/Symbol a}=0.15',"
                    " '../data/data' u 3 w l title '{/Symbol a}=0.5',"
                    " '../data/data' u 4 w l title '{/Symbol a}=1'");
    gp.sendEndOfData(1);
}

#endif // MAIN_H

