#ifndef MAIN_H
#define MAIN_H

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <vector>
#include <cmath>

#include "gnuplot.h"


bool bernouliTry(double p)
{
    double RandResult = static_cast<double>( rand() ) / static_cast<double>( RAND_MAX );
    return RandResult <= p;
}

int geometricDistribution(double p)
{
    int NumberOfSteps=1;
    while(bernouliTry(p)==false){
        NumberOfSteps++;
        if(NumberOfSteps>10000){ return 0; }
    }
    return NumberOfSteps;
}

int streamProbeA(double p, int n)
{
    int LastProbedValue=0;

    for(int i=1; i<=n; i++){
        if(bernouliTry(p)==true){
            LastProbedValue = i;
        }
    }
    return LastProbedValue;
}

int streamProbeB(int n)
{
    int LastProbedValue=0;

    for(int i=1; i<=n; i++){
        if(bernouliTry( 1.0/static_cast<double>(i) )==true){
            LastProbedValue = i;
        }
    }
    return LastProbedValue;
}

std::vector<double> experimentBasic(double p, int n)
{
    std::vector<double> Histogram{};
    int RandResult;

    for(int i=0; i<n; i++){
        RandResult = geometricDistribution(p);
        if(static_cast<unsigned int>(RandResult) >= Histogram.size()){
            Histogram.resize(RandResult+1, 0);
        }
        Histogram[RandResult]+=1.0/static_cast<double>(n);
    }

    return Histogram;
}

std::vector<double> theoryBasic(double p)
{
    std::vector<double> Histogram{};

    for(int i=0; i<1010; i++){
        Histogram.push_back( pow((1.0-p),i-1)*p );
    }

    return Histogram;
}

std::vector<double> experimentStreamProbeA(double p, int n)
{
    std::vector<double> Histogram{};
    int ProbeResult;

    for(int i=0; i<n; i++){
        ProbeResult = streamProbeA(p,100);
        if(static_cast<unsigned int>(ProbeResult) >= Histogram.size()){
            Histogram.resize(ProbeResult+1, 0);
        }
        Histogram[ProbeResult]+=1.0/static_cast<double>(n);
    }

    return Histogram;
}

std::vector<double> theoryStreamProbeA(double p)
{
    std::vector<double> Histogram{};

    for(int i=0; i<1010; i++){
        Histogram.push_back( pow((1.0-p),(100-i))*p );
    }

    return Histogram;
}

std::vector<double> experimentStreamProbeB(int n)
{
    std::vector<double> Histogram{};
    int ProbeResult;

    for(int i=0; i<n; i++){
        ProbeResult = streamProbeB(100);
        if(static_cast<unsigned int>(ProbeResult) >= Histogram.size()){
            Histogram.resize(ProbeResult+1, 0);
        }
        Histogram[ProbeResult]+=1.0/static_cast<double>(n);
    }

    return Histogram;
}

std::vector<double> theoryStreamProbeB()
{
    std::vector<double> Histogram{};

    for(int i=0; i<1010; i++){
        Histogram.push_back( 1.0/100 );
    }

    return Histogram;
}


void saveDataToFile(std::vector<double> experimentalHistogram, std::vector<double> theoreticalHistogram, std::string fileName )
{
    std::fstream file;
    file.open(fileName.c_str(), std::ios::out);

    for(unsigned int i=1; i<experimentalHistogram.size(); i++){
        file << i << "     "
             << theoreticalHistogram.at(i) << "     "
             << experimentalHistogram.at(i) << "     "
             << std::endl;
    }

    file.close();
}

void plotData(GnuplotPipe& gp1, GnuplotPipe& gp2, GnuplotPipe& gp3)
{
    gp1.sendLine("set title 'Geometric distribution'");
    gp1.sendLine("set xlabel 'x'");
    gp1.sendLine("set ylabel 'P(X=x)");
    gp1.sendLine("unset key");
    gp1.sendLine("plot '../data/GD.dat' u 1:2 w l, '../data/GD.dat' u 1:3 w points pointtype 7");
    gp1.sendEndOfData(1);

    gp2.sendLine("set title 'Stream probe P=const'");
    gp2.sendLine("set xlabel 'x'");
    gp2.sendLine("set ylabel 'P(X=x)");
    gp2.sendLine("unset key");
    gp2.sendLine("plot '../data/PA.dat' u 1:2 w l, '../data/PA.dat' u 1:3 w points pointtype 7");
    gp2.sendEndOfData(1);

    gp3.sendLine("set title 'Stream probe P=1/k'");
    gp3.sendLine("set xlabel 'x'");
    gp3.sendLine("set ylabel 'P(X=x)");
    gp3.sendLine("unset key");
    gp3.sendLine("set yrange[0:0.02]");
    gp3.sendLine("plot '../data/PB.dat' u 1:2 w l, '../data/PB.dat' u 1:3 w points pointtype 7");
    gp3.sendEndOfData(1);

    getchar();
}

void experiment(double probability, int numberOfExperiments){
    std::vector<double> GeometricDistributionHistogram = experimentBasic( probability, numberOfExperiments );
    std::vector<double> GeometricDistributionTheoreticalHistogram = theoryBasic(probability);

    std::vector<double> StreamProbeAHistogram = experimentStreamProbeA( probability, numberOfExperiments );
    std::vector<double> StreamProbeATheoreticalHistogram = theoryStreamProbeA( probability );

    std::vector<double> StreamProbeBHistogram = experimentStreamProbeB( numberOfExperiments );
    std::vector<double> StreamProbeBTheoreticalHistogram = theoryStreamProbeB( );

    saveDataToFile(GeometricDistributionHistogram, GeometricDistributionTheoreticalHistogram, "../data/GD.dat");
    saveDataToFile(StreamProbeAHistogram, StreamProbeATheoreticalHistogram,  "../data/PA.dat");
    saveDataToFile(StreamProbeBHistogram, StreamProbeBTheoreticalHistogram,  "../data/PB.dat");
}

#endif // MAIN_H

