#include "main.h"


int main()
{
    GnuplotPipe Gp1;
    GnuplotPipe Gp2;
    GnuplotPipe Gp3;
    double Probability = 0.25;
    int NumberOfExperiments = 1000000;
    srand (time(NULL));

    while(1){
        std::cout << "Probability: ";
        std::cin >> Probability;
        if(Probability<=0){ break; }
        std::cout << "NumberOfExperiments: ";
        std::cin >> NumberOfExperiments;

        experiment(Probability, NumberOfExperiments);
        plotData(Gp1, Gp2, Gp3);
    }

    return 0;
}

