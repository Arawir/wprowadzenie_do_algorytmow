#include "insertsort.h"

using namespace std;

void writeArray(float array[], int length){
    for(int i=1; i<length; i++){
        cout << array[i] << "  ";
    }
}

long long int sortArray(float array[], int length){
    int j;
    float tmp;
    long long int numberOfSteps=0;

    for(int i=1 ;i<length; i++){          numberOfSteps+=2;
        j = i;                            numberOfSteps+=1;
        tmp = array[i];                   numberOfSteps+=1;
        while(array[j-1]>tmp){            numberOfSteps+=1;
            array[j] = array[j-1];        numberOfSteps+=1;
            j--;                          numberOfSteps+=1;
        }
        array[j] = tmp;                   numberOfSteps+=1;
    }

    return numberOfSteps;
}

void generateRandomArray(float array[], int length){

    array[0] = std::numeric_limits<float>::lowest();
    for(int i=1; i<length; i++){
        array[i] = static_cast<float>(rand()%2000)/100.0-10.0;
    }
}

double calulateSumOfArray(float array[], int length){
    double sum=0;
    for(int i=1; i<length; i++){
        sum = sum + array[i];
    }
    return sum;
}

bool arrayIsSorted(float array[], int length){
    for(int i=1; i<length; i++){
        if(array[i]<array[i-1]){
            return false;
        }
    }
    return true;
}

void analizeResults(float array[], int length, double expectedSumOfArray){
    if(calulateSumOfArray(array, length) != expectedSumOfArray){
        cout << "Program lost data!";
        cout << "Input: "; writeArray(array,length); cout << endl;
        cout << "Output: "; writeArray(array,length); cout << endl;
    } else if (!arrayIsSorted(array, length)){
        cout << "Array is no sorted correctly!";
        cout << "Input: "; writeArray(array,length); cout << endl;
        cout << "Output: "; writeArray(array,length); cout << endl;
    }
}

void generateBestCaseArray(float array[], int length)
{
    array[0] = std::numeric_limits<float>::lowest();
    for(int i=1; i<length; i++){
        array[i] = static_cast<float>(i);
    }
}

void generateWorstCaseArray(float array[], int length)
{
    array[0] = std::numeric_limits<float>::lowest();
    for(int i=1; i<length; i++){
        array[i] = static_cast<float>(-i);
    }
}

long long int calculateNumberOfStepsInBestCase(int length)
{
    double sumOfArray=0;
    float *Data;
    long long int numberOfSteps=0;
    Data = new float[length];
    generateBestCaseArray(Data, length);
    sumOfArray = calulateSumOfArray(Data,length);
    numberOfSteps = sortArray(Data, length);
    analizeResults(Data, length, sumOfArray);
    delete[] Data;
    return numberOfSteps;
}

long long int calculateNumberOfStepsInWorstCase(int length)
{
    double sumOfArray=0;
    float *Data;
    long long int numberOfSteps=0;
    Data = new float[length];
    generateWorstCaseArray(Data, length);
    sumOfArray = calulateSumOfArray(Data,length);
    numberOfSteps = sortArray(Data, length);
    analizeResults(Data, length, sumOfArray);
    delete[] Data;
    return numberOfSteps;
}

long long int calculateNumberOfStepsInRandomCase(int length)
{
    double sumOfArray=0;
    float *Data;
    long long int numberOfSteps=0;
    Data = new float[length];
    generateRandomArray(Data, length);
    sumOfArray = calulateSumOfArray(Data,length);
    numberOfSteps = sortArray(Data, length);
    analizeResults(Data, length, sumOfArray);
    delete[] Data;
    return numberOfSteps;
}

void generateHistogramOfSetpsNumber(long long numberOfStepsBestCase,
                                    long long numberOfStepsWorstCase,
                                    long long numberOfStepsArray[],
                                    int lengthOfArray,
                                    int lengthOfInputData)
{
    fstream plik;
    plik.open( to_string(lengthOfInputData).c_str(), std::ios::out );

    int histogram[101];
    int distance = (numberOfStepsWorstCase - numberOfStepsBestCase)/100;

    for(int i=0 ;i<100; i++){
        histogram[i]=0;
    }

    for(int i=0; i<lengthOfArray; i++){
        histogram[ (numberOfStepsArray[i]-numberOfStepsBestCase)/distance ]++;
    }

    for(int i=0 ;i<100; i++){
        plik << numberOfStepsBestCase + distance*i << "      " << histogram[i] << endl;
    }

    plik.close();
}

long long calulateAvarrageValue(long long Array[], int lengthOfArray)
{
    long long Value = 0;
    for(int i=0; i<lengthOfArray; i++){
        Value+= Array[i];
    }
    return Value/lengthOfArray;
}
