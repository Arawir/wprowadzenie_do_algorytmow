#ifndef INSERTSORT
#define INSERTSORT


#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <string>
#include <limits>


void writeArray(float array[], int length);
long long int sortArray(float array[], int length);
void generateRandomArray(float array[], int length);
void generateBestCaseArray(float array[], int length);
void generateWorstCaseArray(float array[], int length);
double calulateSumOfArray(float array[], int length);
bool arrayIsSorted(float array[], int length);
void analizeResults(float array[], int length, double expectedSumOfArray);

long long int calculateNumberOfStepsInBestCase(int length);
long long int calculateNumberOfStepsInWorstCase(int length);
long long int calculateNumberOfStepsInRandomCase(int length);

long long int calulateAvarrageValue(long long int Array[], int lengthOfArray);
void generateHistogramOfSetpsNumber(long long int numberOfStepsBestCase,
                                    long long int numberOfStepsWorstCase,
                                    long long int numberOfStepsArray[],
                                    int lengthOfArray,
                                    int lengthOfInpoutData);
#endif // INSERTSORT

