#include "insertsort.h"
#include <cmath>


int main(){

    std::fstream plik;
    plik.open("timings", std::ios::out);

    int numberOfSamples = 1000;
    srand( time(NULL) );

    long long int numberOfStepsBestCase;
    long long int numberOfStepsWorstCase;
    long long int* numberOfStepsRandom = new long long int[numberOfSamples];

    for(int length=50; length<=1000; length+=50){
        std::cout << "Length: " << length << std::endl;
        numberOfStepsBestCase = calculateNumberOfStepsInBestCase(length);
        numberOfStepsWorstCase = calculateNumberOfStepsInWorstCase(length);
        for(int i=0; i<numberOfSamples; i++){
           numberOfStepsRandom[i] = calculateNumberOfStepsInRandomCase(length);
        }

        plik << length << "     "
             << numberOfStepsBestCase << "     "
             << calulateAvarrageValue(numberOfStepsRandom, numberOfSamples) << "     "
             << sqrt( calulateAvarrageValue(numberOfStepsRandom, numberOfSamples) ) << "     "
             << numberOfStepsWorstCase << "     "
             << sqrt( numberOfStepsWorstCase ) << "     "
             << std::endl;
        generateHistogramOfSetpsNumber(numberOfStepsBestCase,
                                       numberOfStepsWorstCase,
                                       numberOfStepsRandom,
                                       numberOfSamples,
                                       length);
    }



    plik.close();
    return 0;
}

