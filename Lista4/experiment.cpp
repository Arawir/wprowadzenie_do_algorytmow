#include "experiment.h"
#include <stdlib.h>

Experiment::Experiment(std::mt19937& generator) :
    RandomNumberGenerator{generator}
  , Boxes{nullptr}
  , N{0}
{

}

Experiment::~Experiment()
{
    if(Boxes != nullptr){
        delete[] Boxes;
    }
}

void Experiment::setSizeAndClear(int n)
{
    N = n;
    if(Boxes != nullptr){
        delete[] Boxes;
    }
    Boxes = new int[N];
    for(int i=0; i<N; i++){
        Boxes[i]=0;
    }
}

void Experiment::randBalls(int numberOfBalls)
{
    std::uniform_int_distribution<int> Distribution(0, N-1);
    for(int i=0; i<numberOfBalls; i++){
        Boxes[ Distribution(RandomNumberGenerator) ]++;
    }
}

bool Experiment::checkA()
{
    for(int i=0; i<N; i++){
        if(Boxes[i]==0){ return false; }
    }
    return true;
}

bool Experiment::checkB()
{
    for(int i=0; i<N; i++){
        if(Boxes[i]>1){ return true; }
    }
    return false;
}

double Experiment::emptyLevel()
{
    double Out=0;
    for(int i=0; i<N; i++){
        if(Boxes[i]==0){ Out+=1.0; }
    }
    return Out/static_cast<double>(N);
}

void Experiment::write(std::ostream& stream)
{
    for(int i=0; i<N; i++){
        stream << Boxes[i] << "   ";
    }
    stream.flush();
}


