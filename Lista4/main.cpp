#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "experiment.h"
#include <vector>
#include <fstream>
#include <chrono>
#include "gnuplot.h"

std::vector<double> generateTheoreticalCumulativeDistributionB(int n){
    std::vector<double> Probability;
    std::vector<double> CumulativeDistribution;
    double Pk=0.0;
    double Dk=0.0;

    Probability.push_back(0.0);
    CumulativeDistribution.push_back(0.0);
    Probability.push_back(1.0/n);
    CumulativeDistribution.push_back(1.0/n);
    Dk = 1.0/n;

    for(int k=3; k<=n+1; k++){
        Pk = Probability.back()*static_cast<double>((k-1)*(n-k+2)) / static_cast<double>(n*(k-2));
        Dk = Dk + Pk;
        Probability.push_back(Pk);
        CumulativeDistribution.push_back(Dk);
    }

    return CumulativeDistribution;
}

std::vector<double> generateTheoreticalCumulativeDistributionA(int n){
    double Tab[n];
    double TabP[n];
    std::vector<double> P;

    for(int i=0; i<n; i++){
        Tab[i]=0.0;
        TabP[i]=0;
    }
    Tab[0] = 1.0;
    P.push_back(0.0);

    while(Tab[n-1]<0.999){
        TabP[0] = Tab[0]*1.0/static_cast<double>(n);
        for(int j=1; j<n; j++){
            TabP[j] = Tab[j] *static_cast<double>(j+1)/static_cast<double>(n)
                    + Tab[j-1]*static_cast<double>(n-j)/static_cast<double>(n);
        }
        for(int j=0; j<n; j++){
            Tab[j] = TabP[j];
        }

        P.push_back(Tab[n-1]);
    }

    return P;
}






double distributionA(std::vector<Experiment> &experiments){
    double out=0;
    for(auto& experiment : experiments){
        if(experiment.checkA()) out+=1.0;
    }
    return out/experiments.size();
}

double distributionB(std::vector<Experiment> &experiments){
    double out=0;
    for(auto& experiment : experiments){
        if(experiment.checkB()) out+=1.0;
    }
    return out/experiments.size();
}

double averrrageEmptyLevel(std::vector<Experiment> &experiments){
    double Out=0;
    for(auto& experiment : experiments){
        Out+=experiment.emptyLevel();
    }
    return Out/experiments.size();
}

void randBalls(std::vector<Experiment> &experiments, int numberOfBalls){
    for(auto& experiment : experiments){
        experiment.randBalls(numberOfBalls);
    }
}

void resizeExperiments(std::vector<Experiment> &experiments, int newSize){
    for(auto& experiment : experiments){
        experiment.setSizeAndClear(newSize);
    }
}

void prepareExperiments(std::vector<Experiment> &experiments, std::mt19937 &generator, int numberOfExperiments){
    for(int i=0; i<numberOfExperiments; i++){
        experiments.push_back(Experiment{generator});
    }
}

int examineA(int NumberOfBoxes, int NumberOfExperiments){
    std::fstream file;
    std::vector<double> CumulativeDistributionA = generateTheoreticalCumulativeDistributionA(NumberOfBoxes);
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937 Generator(seed);
    std::vector<Experiment> Experiments;

    int Out=0;

    prepareExperiments(Experiments, Generator, NumberOfExperiments);
    resizeExperiments(Experiments, NumberOfBoxes);

    randBalls(Experiments, 1);
    file.open("../data/A", std::ios::out);
    for(auto distA : CumulativeDistributionA){
        if(distributionA(Experiments)<0.5){
            Out++;
        }
        file << distA << "    "
             << distributionA(Experiments) << "     "
             << std::endl;
        randBalls(Experiments, 1);
    }
    file.close();

    return Out;
}

int examineB(int NumberOfBoxes, int NumberOfExperiments){
    std::fstream file;
    std::vector<double> CumulativeDistributionB = generateTheoreticalCumulativeDistributionB(NumberOfBoxes);
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937 Generator(seed);

    std::vector<Experiment> Experiments;
    int Out=0;


    prepareExperiments(Experiments, Generator, NumberOfExperiments);
    resizeExperiments(Experiments, NumberOfBoxes);

    randBalls(Experiments, 1);
    file.open("../data/B", std::ios::out);
    for(auto distB : CumulativeDistributionB){
        if(distributionB(Experiments)<0.5){
            Out++;
        }
        file << distB << "     "
             << distributionB(Experiments) << "     "
             << std::endl;
        randBalls(Experiments, 1);
    }
    file.close();
    return Out;
}

double examineC(int NumberOfBoxes, int NumberOfExperiments){
    std::fstream file;
    std::vector<double> CumulativeDistributionB = generateTheoreticalCumulativeDistributionB(NumberOfBoxes);
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937 Generator(seed);

    std::vector<Experiment> Experiments;


    prepareExperiments(Experiments, Generator, NumberOfExperiments);
    resizeExperiments(Experiments, NumberOfBoxes);

    randBalls(Experiments, NumberOfBoxes);

    return averrrageEmptyLevel(Experiments);
}

void plotDataA(){
    GnuplotPipe gp;
    gp.sendLine("set title 'A'");
    gp.sendLine("plot '../data/A' u 1 w l, '../data/A' u 2 w points pointtype 7");
    gp.sendEndOfData(1);
}

void plotDataB(){
    GnuplotPipe gp;
    gp.sendLine("set title 'B'");
    gp.sendLine("plot '../data/B' u 1 w l, '../data/B' u 2 w points pointtype 7");
    gp.sendEndOfData(1);
}

void plotDataNa(){
    GnuplotPipe gp;
    gp.sendLine("set title 'N_A'");
    gp.sendLine("plot '../data/N' u 1:2 w l, '../data/N' u 1:3 w points pointtype 7");
    gp.sendEndOfData(1);
}

void plotDataNb(){
    GnuplotPipe gp;
    gp.sendLine("set title 'N_B'");
    gp.sendLine("plot '../data/N' u 1:4 w l, '../data/N' u 1:5 w points pointtype 7");
    gp.sendEndOfData(1);
}

void plotDataNc(){
    GnuplotPipe gp;
    gp.sendLine("set title 'N_C'");
    gp.sendLine("plot '../data/N' u 1:6 w points pointtype 7");
    gp.sendEndOfData(1);
}

int main()
{
    std::fstream file;
//    file.open("../data/N", std::ios::out);
//    for(int N=10; N<400; N+=10){
//        file << N << "        "
//             << N*std::log(static_cast<double>(N))+0.58*N << "     "
//             << examineA(N,1000) << "        "
//             << sqrt( 2.0*log(2.0)*static_cast<double>(N) ) << "    "
//             << examineB(N,1000) << "        "
//             << examineC(N,1000) << "    "
//             << std::endl;
//        std::cout << N << std::endl;
//    }
//    file.close();

    plotDataA();
    plotDataB();
    plotDataNa();
    plotDataNb();
    plotDataNc();
    return 0;
}

