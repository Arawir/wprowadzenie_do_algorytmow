#ifndef EXPERIMENT_H
#define EXPERIMENT_H

#include <random>
#include <iostream>

class Experiment{
public:
    Experiment(std::mt19937& generator);
    ~Experiment();

    void setSizeAndClear(int n);
    void randBalls(int numberOfBalls);

    bool checkA();
    bool checkB();
    double emptyLevel();

    void write(std::ostream& stream);
public:
    std::mt19937& RandomNumberGenerator;
    int *Boxes;
    int N;
};

#endif // EXPERIMENT_H
