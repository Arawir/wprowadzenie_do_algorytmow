#ifndef NUMBERMODULE_H
#define NUMBERMODULE_H

#include <iostream>
#include <cstdint>
#include <string>

class NumberModule{
public:
    NumberModule();
    NumberModule(unsigned int value);
    NumberModule(const NumberModule &value);
    NumberModule(const std::string &value);
    ~NumberModule() = default;

    unsigned int getValue() const;

    NumberModule& operator= (const NumberModule &other);
    NumberModule& operator= (const unsigned int &other);
    NumberModule& operator= (const std::string &other);
    NumberModule operator+ (const NumberModule &other);
    NumberModule operator- (const NumberModule &other);
    NumberModule operator* (const NumberModule &other);
    NumberModule operator>> (const int length);
    NumberModule operator<< (const int length);

    bool operator== (const NumberModule &other);
    bool operator!= (const NumberModule &other);
    bool operator> (const NumberModule &other);
    bool operator< (const NumberModule &other);
    bool operator>= (const NumberModule &other);
    bool operator<= (const NumberModule &other);

    void resizeToLength(int length);

    std::string Array;

private:
    void resizeToUsedSpace();

    void move(int length);
};

std::ostream & operator<< (std::ostream &stream, const NumberModule &other);
std::ostream & operator>> (std::istream &stream, NumberModule &other);

#endif // NUMBERMODULE_H
