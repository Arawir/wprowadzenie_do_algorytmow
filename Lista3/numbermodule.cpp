#include "numbermodule.h"
#include <cmath>
#include <iomanip>

NumberModule::NumberModule() :
    Array{"0"}
{
}

NumberModule::NumberModule(unsigned int value)
{
    if(value==0){
        Array.push_back(0);
    }
    while(value>0){
        Array.push_back(static_cast<char>(value%10));
        value /= 10;
    }
}

NumberModule::NumberModule(const NumberModule &value) :
    Array{value.Array}
{

}

NumberModule::NumberModule(const std::__cxx11::string &value) :
    Array{value}
{

}

unsigned int NumberModule::getValue() const
{
    unsigned int Value=0;
    for(int i=0; i<Array.size(); i++){
        Value += static_cast<unsigned int>(Array[i])*pow(10, i);
    }
    return Value;
}

NumberModule& NumberModule::operator=(const NumberModule &other)
{
    Array.clear();
    Array = other.Array;
    return *this;
}

NumberModule &NumberModule::operator=(const unsigned int &other)
{
    unsigned int value = other;
    Array.clear();
    if(value==0){
        Array.push_back(0);
    }
    while(value>0){
        Array.push_back(static_cast<char>(value%10));
        value /= 10;
    }
}

NumberModule &NumberModule::operator=(const std::__cxx11::string &other)
{
    Array.clear();
    for(int i=other.size()-1; i>=0; i--){
        Array.push_back(other[i]-'0');
    }
    return *this;
}

NumberModule NumberModule::operator+(const NumberModule &other)
{
    NumberModule Out{0};
    int TemporarySum;

    int Size = std::max(Array.size(), other.Array.size())+1;

    for(unsigned int i=0; i<Size; i++){
        TemporarySum = Out.Array.at(i);
        if(i<Array.size()) TemporarySum += Array.at(i);
        if(i<other.Array.size()) TemporarySum += other.Array.at(i);
        Out.Array[i] = TemporarySum%10;
        Out.Array.push_back( TemporarySum/10 );
    }

    Out.resizeToUsedSpace();
    return Out;
}

NumberModule NumberModule::operator-(const NumberModule &other)
{
    NumberModule Out{};
    int TmpSub;

    if(*this >= other){
        Out.Array = Array;
        for(int i=Out.Array.size()-1; i>0; i--){
            Out.Array[i] = Out.Array[i]-1;
            Out.Array[i-1] = Out.Array[i-1]+10;
        }
        for(int i=0; i<Out.Array.size(); i++){
            TmpSub = Out.Array[i];
            if(i<other.Array.size()){ TmpSub-=other.Array[i]; }
            Out.Array[i] = TmpSub%10;
            if((i+1)<Out.Array.size()){ Out.Array[i+1] += TmpSub/10; }
        }
    } else {
        Out.Array = other.Array;
        for(int i=Out.Array.size()-1; i>0; i--){
            Out.Array[i] = other.Array[i]-1;
            Out.Array[i-1] = other.Array[i-1]+10;
        }
        for(int i=0; i<Out.Array.size()-1; i++){
            TmpSub = Out.Array[i];
            if(i<Array.size()){ TmpSub-=Array[i]; }
            Out.Array[i] = TmpSub%10;
            if((i+1)<Out.Array.size()){ Out.Array[i+1] += TmpSub/10; }
        }

    }

    Out.resizeToUsedSpace();
    return Out;
}

NumberModule NumberModule::operator*(const NumberModule &other)
{
    NumberModule Out{};
    NumberModule A = (*this);
    NumberModule B = other;

    if( (A.Array.size()<2) && (B.Array.size()<2) ){
        Out = A.getValue()*B.getValue();
        return Out;
    }

    int Size = std::max(Array.size(), other.Array.size());
    A.resizeToLength(Size);
    B.resizeToLength(Size);

    int N = (Size+1)/2;

    NumberModule Al = A.Array.substr(N, Size);
    NumberModule Ar = A.Array.substr(0, N);
    NumberModule Bl = B.Array.substr(N, Size);
    NumberModule Br = B.Array.substr(0, N);

    NumberModule AlBl = Al*Bl;
    NumberModule ArBr = Ar*Br;

    NumberModule Al_p_Ar = Al+Ar;
    NumberModule Bl_p_Br = Bl+Br;

    NumberModule AlBr_p_ArBl = Al_p_Ar*Bl_p_Br;
    AlBr_p_ArBl = AlBr_p_ArBl-AlBl;
    AlBr_p_ArBl = AlBr_p_ArBl-ArBr;

    AlBl.move(2*N);
    AlBr_p_ArBl.move(N);

//    std::cout << "A: " << A << "     "
//              << "B: " << B << "     "
//              << "Al: " << Al << "     "
//              << "Ar: " << Ar << "     "
//              << "Bl: " << Bl << "     "
//              << "Br: " << Br << "     "
//              << "AlBl: " << AlBl << "    "
//              << "ArBr: " << ArBr << "    "
//              << "sum: " << AlBr_p_ArBl << "    "
//              << std::endl << std::endl;

    Out = AlBl + AlBr_p_ArBl + ArBr;
    Out.resizeToUsedSpace();
    return Out;
}

bool NumberModule::operator==(const NumberModule &other)
{
    return Array == other.Array;
}

bool NumberModule::operator!=(const NumberModule &other)
{
    return Array != other.Array;
}

bool NumberModule::operator>(const NumberModule &other)
{
    if(Array.size() > other.Array.size()) return true;
    if(Array.size() < other.Array.size()) return false;

    for(int i=Array.size()-1;i>=0; i--){
        if(Array[i]>other.Array[i]) return true;
    }
    return false;
}

bool NumberModule::operator<(const NumberModule &other)
{
    return !(*this >= other);
}

bool NumberModule::operator>=(const NumberModule &other)
{
    return ((*this>other) || (*this==other));
}

bool NumberModule::operator<=(const NumberModule &other)
{
    return !(*this > other);
}

void NumberModule::resizeToUsedSpace()
{
    while((Array.back()==0)&&(Array.size()>1)){
        Array.pop_back();
    }
}

void NumberModule::move(int length)
{
    if(length<0){
        if(length>=Array.size()){
            (*this) = 0;
        } else {
            for(int i=0; i<length; i++){
                Array.erase(0);
            }
        }
    } else {
        for(int i=0; i<length; i++){
            Array = static_cast<char>(0) + Array;
        }
    }

    resizeToUsedSpace();
}

void NumberModule::resizeToLength(int length)
{
    //if(length%2==1){ length+=1; }
    if(length>=Array.size()){
        while(length!=Array.size()){
            Array.push_back(0);
        }
    } else {
        while(length==Array.size()){
            Array.pop_back();
        }
    }
}




std::ostream &operator<<(std::ostream &stream, const NumberModule &other)
{
    for(int i=other.Array.size()-1; i>=0; i--){
        stream << static_cast<char>(other.Array[i] + '0');
        if((i!=0)&&(i%3==0)){ stream << "'"; }
    }
    return stream;
}

std::ostream &operator>>(std::istream &stream, NumberModule &other)
{
    std::string tmp;
    stream >> tmp;
    other = tmp;
}
