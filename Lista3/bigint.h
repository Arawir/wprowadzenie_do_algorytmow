#ifndef BIGINT_H
#define BIGINT_H

#include "numbermodule.h"

class BigInt : public NumberModule{
public:
    BigInt();
    BigInt(int value);
    BigInt(std::string value);
    ~BigInt() = default;

    BigInt& operator= (const BigInt &other);

    BigInt operator+ (const BigInt &other);
    BigInt operator- (const BigInt &other);
    BigInt operator* (BigInt &other);

    bool operator== (const BigInt &other);
    bool operator!= (const BigInt &other);
    bool operator> (const BigInt &other);
    bool operator< (const BigInt &other);
    bool operator>= (const BigInt &other);
    bool operator<= (const BigInt &other);

    char Sign;
};

std::ostream & operator<< (std::ostream &stream, const BigInt &other);
std::istream & operator>> (std::istream &stream, BigInt &other);

#endif // BIGINT_H
