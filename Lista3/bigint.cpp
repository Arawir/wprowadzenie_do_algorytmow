#include "bigint.h"
#include <cmath>
#include <iomanip>

BigInt::BigInt() :
    NumberModule{}
  , Sign{'+'}
{

}

BigInt::BigInt(int value) :
    NumberModule{std::abs(value)}
  , Sign{'+'}
{
    if(value<0) Sign = '-';
}

BigInt::BigInt(std::__cxx11::string value) :
    NumberModule{}
  , Sign{'+'}
{
    if(value[0]=='+'){
        Sign = '+';
        value.erase(0,0);
    } else if(value[0]=='-'){
        Sign = '-';
        value.erase(0,0);
    }

    NumberModule::operator = (value);
}

BigInt& BigInt::operator=(const BigInt &other)
{
    Sign = other.Sign;
    NumberModule::operator=(static_cast<NumberModule>(other));
    return *this;
}

BigInt BigInt::operator+(const BigInt &other)
{
    BigInt Out{};

    if(Sign=='+'){
        if(other.Sign=='+'){
            Out.Sign = '+';
            static_cast<NumberModule>(Out).operator =(NumberModule::operator +(static_cast<NumberModule>(other)));
        } else {
            if((*this)>=other){
                Out.Sign = '+';
                static_cast<NumberModule>(Out).operator =(NumberModule::operator -(static_cast<NumberModule>(other)));
            } else {
                Out.Sign = '-';
                static_cast<NumberModule>(Out).operator =(static_cast<NumberModule>(other).operator -(static_cast<NumberModule>(*this)));
            }
        }
    } else {
        if(other.Sign=='-'){
            Out.Sign = '-';
            static_cast<NumberModule>(Out).operator =(NumberModule::operator +(static_cast<NumberModule>(other)));
        } else {
            if((*this)>=other){
                Out.Sign = '-';
                static_cast<NumberModule>(Out).operator =(NumberModule::operator -(static_cast<NumberModule>(other)));
            } else {
                Out.Sign = '+';
                static_cast<NumberModule>(Out).operator =(static_cast<NumberModule>(other).operator -(static_cast<NumberModule>(*this)));
            }
        }
    }
    return Out;
}

//BigInt BigInt::operator-(const BigInt &other)
//{

//}

//BigInt BigInt::operator*(BigInt &other)
//{
//   // NumberModule::operator*(static_cast<NumberModule&>(other));
//    return *this;
//}

bool BigInt::operator==(const BigInt &other)
{
    return NumberModule::operator==(static_cast<NumberModule>(other));
}

bool BigInt::operator!=(const BigInt &other)
{
    return !((*this)==other);
}

bool BigInt::operator>(const BigInt &other)
{
    if(Sign=='+'){
        if(other.Sign=='+') return static_cast<NumberModule>(*this) > static_cast<NumberModule>(other);
        if(other.Sign=='-') return true;
    } else {
        if(other.Sign=='-') return static_cast<NumberModule>(*this) < static_cast<NumberModule>(other);
        if(other.Sign=='+') return false;
    }
}

bool BigInt::operator<(const BigInt &other)
{
    //return !((*this)>other)||(*this)==other));
}

bool BigInt::operator>=(const BigInt &other)
{
    return ~((*this)<other);
}

bool BigInt::operator<=(const BigInt &other)
{
    return ~((*this)>other);
}

std::ostream &operator<<(std::ostream &stream, const BigInt &other)
{
    if(other.Sign != '+'){
        stream << other.Sign;
    }
 //   stream << static_cast<NumberModule>(other);
    return stream;
}

std::istream &operator>>(std::istream &stream, BigInt &other)
{
    std::string tmp;
    stream >> tmp;
    other = tmp;
    return stream;
}
