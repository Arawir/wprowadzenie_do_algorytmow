TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    bigint.cpp \
    numbermodule.cpp

HEADERS += \
    bigint.h \
    numbermodule.h

