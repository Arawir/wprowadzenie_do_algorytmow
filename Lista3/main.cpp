#include <iostream>
#include "bigint.h"

using namespace std;

int main()
{
    NumberModule A;
    NumberModule B;

    while(1){
        std::cout << "///////////////////////////////////////////" << std::endl;
        std::cout << "Podaj A: ";
        std::cin >> A;
        std::cout << "Podaj B: ";
        std::cin >> B;

        std::cout << A << "  +  "
                  << B << "  =  "
                  << A+B << "  "
                  << std::endl;

        std::cout << A << "  -  "
                  << B << "  =  "
                  << A-B << "  "
                  << std::endl;

        std::cout << A << "  *  "
                  << B << "  =  "
                  << A*B << "  "
                  << std::endl;
    }




    return 0;
}

