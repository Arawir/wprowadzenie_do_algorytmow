#ifndef IPROBLEM_H
#define IPROBLEM_H

#include <iostream>

class iProblem{
public:
    iProblem();
    virtual ~iProblem() = default;
    virtual void setSize(int size) = 0;
    virtual void setDebugMode() = 0;
    virtual void unsetDebugMode() = 0;
    virtual int numberOfStepsRandomCase() = 0;
    virtual int numberOfStepsBestCase() = 0;
    virtual int numberOfStepsWorstCase() = 0;
    virtual std::string getName() = 0;

    virtual void setPosition(int position) = 0; //ugly...
};

#endif // IPROBLEM_H
