#ifndef STATISTICSCRERATOR_H
#define STATISTICSCRERATOR_H

#include "iproblem.h"

class StatisticsCrerator{
public:
    StatisticsCrerator(iProblem& problem);
    bool checkParametersCorrectnes();
    void prapeareStatistics();
public:
    int MinProblemSize;
    int MaxProblemSize;
    int ProblemSizeStep;
    int NumberOfSamples;
private:
    iProblem& Problem;

};

#endif // STATISTICSCRERATOR_H
