TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    statisticscrerator.cpp \
    iproblem.cpp \
    quickselect.cpp

HEADERS += \
    statisticscrerator.h \
    iproblem.h \
    quickselect.h

