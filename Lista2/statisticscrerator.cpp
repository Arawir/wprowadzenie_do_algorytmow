#include "statisticscrerator.h"
#include <fstream>
#include "quickselect.h"

StatisticsCrerator::StatisticsCrerator(iProblem& problem) :
    Problem(problem)
{

}

bool StatisticsCrerator::checkParametersCorrectnes()
{

}

void StatisticsCrerator::prapeareStatistics()
{
    std::fstream plik_histogram;
    std::fstream plik_complexity;

    plik_complexity.open("../data/" + Problem.getName()+"_complexity.data", std::ios::out);

    for(int ProblemSize = MinProblemSize; ProblemSize<= MaxProblemSize; ProblemSize+=ProblemSizeStep){
        Problem.setSize(ProblemSize);
        long long int tmp = 0;
        for(int i=0; i<NumberOfSamples; i++){
           Problem.setPosition(std::rand()%(ProblemSize-2)+1);
           tmp+=Problem.numberOfStepsRandomCase();
        }
        tmp = tmp/NumberOfSamples;
        plik_complexity << ProblemSize << "   " << tmp << std::endl;
    }

    plik_complexity.close();
}

