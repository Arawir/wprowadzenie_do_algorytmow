#include "quickselect.h"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <bits/stdc++.h>

QuickSelect::QuickSelect() :
    iProblem{}
  , DebugMode{false}
  , Size{0}
  , Array{nullptr}
  , Position{-1}
{
    std::srand(std::time(nullptr));
}

void QuickSelect::setSize(int size)
{
    Size = size;
    if(Array != nullptr){
        delete []Array;
    }
    Array = new int[Size];
}

void QuickSelect::setPosition(int position)
{
    Position = position;
}

void QuickSelect::setDebugMode()
{
    DebugMode = true;
}

void QuickSelect::unsetDebugMode()
{
    DebugMode = false;
}

int QuickSelect::numberOfStepsRandomCase()
{
    NumberOfOperations = 0;
    fillArrayRandomNumbers();

    int Element = kthSmallest(0, Size-1, Position);
    checkCorrectness(Element);

    return NumberOfOperations;
}

int QuickSelect::numberOfStepsBestCase()
{

}

int QuickSelect::numberOfStepsWorstCase()
{

}

std::string QuickSelect::getName()
{
    return "QuickSelect";
}

void QuickSelect::fillArrayRandomNumbers()
{
    for(int i=0; i<Size; i++){
        Array[i] = std::rand()%1000;
    }

    if(DebugMode){
        for(int i=0; i<Size; i++){
            std::cout << Array[i] << "  ";
        }
    }
}

int QuickSelect::partitionArray(int left, int right)
{
    int x = Array[right], i = left;            NumberOfOperations+=2;
    for (int j = left; j <= right - 1; j++) {  NumberOfOperations+=2;
        if (Array[j] <= x) {                   NumberOfOperations+=1;
            std::swap(Array[i], Array[j]);     NumberOfOperations+=3;
            i++;                               NumberOfOperations+=1;
        }
    }
    std::swap(Array[i], Array[right]);         NumberOfOperations+=3;
    return i;
}

int QuickSelect::kthSmallest(int left, int right, int k)
{
    if (k > 0 && k <= right - left + 1) {                                     NumberOfOperations+=2;
        int index = partitionArray(left, right);                              NumberOfOperations+=2;
        if (index - left == k - 1){
            return Array[index];
        }
        if (index - left > k - 1){
            return kthSmallest(left, index - 1, k);
        }
        return kthSmallest(index + 1, right, k - index + left - 1);
    }
    return 123;
}

bool QuickSelect::checkCorrectness(int Number)
{
    if(Position < 1){
        std::cout << "FAIL"
                  << " Wrong position settings: " << Position
                  << std::endl;

    }


    int tmp=0;
    int equal=0;
    for(int i=0; i<Size; i++){
        if(Array[i]<Number) tmp++;
        if(Array[i]==Number) equal++;
    }

    if((Position-1>=tmp)&&(Position-1<=tmp+equal)){
        if(DebugMode){
            std::cout << "OK"
                      << " Position: " << Position
                      << " Number: " << Number
                      << std::endl;

        }
        return true;
    }

    std::cout << "FAIL"
              << " Position: " << Position
              << " Number: " << Number
              << std::endl;
    std::sort(Array, Array+Size);
    for(int i=0; i<Size; i++){
        if(i==Position-1){ std::cout << " -->"; }
        std::cout << Array[i] << "  ";
    }
    std::cout << std::endl << std::endl;
    return false;
}

