#ifndef QUICKSELECT_H
#define QUICKSELECT_H

#include "iproblem.h"

class QuickSelect : public iProblem{
public:
    QuickSelect();

    void setSize(int size) override;
    void setPosition(int position);
    void setDebugMode() override;
    void unsetDebugMode() override;
    int numberOfStepsRandomCase() override;
    int numberOfStepsBestCase() override;
    int numberOfStepsWorstCase() override;
    std::string getName() override;
private:
    void fillArrayRandomNumbers();
    int partitionArray(int left, int right);
    int kthSmallest(int l, int r, int k);
    bool checkCorrectness(int Number);
private:
    int Position;
    bool DebugMode;
    int Size;
    int *Array;

    int NumberOfOperations;
};

#endif // QUICKSORT_H
